import os
import asyncio

import aiohttp
from bs4 import BeautifulSoup
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

from dotenv import load_dotenv
from discord.ext import commands

load_dotenv()
token = os.getenv('DISCORD_TOKEN')

bot = commands.Bot(command_prefix='포로야 ')

engine = create_engine('sqlite:///:memory:', echo=True)
Base = declarative_base()


# 이미 크롤링 한 개념글을 봇이 기억해두기 위해 크롤링 된 개념글을 DB에 저장해 놓는 ORM 테이블
class BestArticle(Base):
    __tablename__ = 'best_article'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    url = Column(String)
    gallery = Column(String)

    def __repr__(self):
        return "<BestArticle(title='%s', url='%s', gallery='%s')>" % (self.title, self.url, self.gallery)


@bot.event
async def on_ready():
    # 봇이 처음으로 준비 완료되었을 때 실행되는 on_ready 이벤트 핸들러
    print(f'{bot.user.name} 준비 완료')


@bot.event
async def on_message(message):
    # 루카#8190
    if message.author.id == 330610512213704705:
        # 루카가 메세지 적으면 요리남 이모지를 반응으로 자동 등록
        await message.add_reaction('🍳')
    # on_message 이벤트 핸들러가 on_command 이벤트 처리를 오버라이딩 하기 때문에 반드시 process_commands 를 호출해야 함
    await bot.process_commands(message)


@bot.event
async def on_command_error(ctx, error):
    await ctx.send('무슨 말씀하시는지 모르겠어요 😨')


async def scrapping_task():
    await bot.wait_until_ready()
    print('개념글 스크래핑을 시작')
    # 낚시터 채널
    channel = bot.get_channel(614391993652477954)
    # 마지막으로 스크래핑 한 URL
    last_urls = {'중갤': None, '만갤': None}
    targets = {
        '중갤': 'https://gall.dcinside.com/mgallery/board/lists?id=aoegame&exception_mode=recommend',
        '만갤': 'https://gall.dcinside.com/board/lists?id=comic_new2&exception_mode=recommend',
    }
    # 디씨에서 HTTP 요청 시 User-Agent 값을 통한 클라이언트 검증을 하기 때문에 최신 크롬 User-Agent를 추가
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}

    while not bot.is_closed():
        async with aiohttp.ClientSession(headers=headers) as session:
            for target_name, target_url in targets.items():
                async with session.get(target_url) as r:
                    if r.status == 200:
                        text = await r.read()
                        soup = BeautifulSoup(text.decode('utf-8'), 'html.parser')
                        # 중갤 개념글의 가장 최신 게시글에 접근하기 위한 CSS Selector는 아래와 같다
                        # table.gall_list > tbody > tr:nth-child(3)
                        # 게시글 목록을 나타내는 테이블
                        table = soup.find('table', {'class': 'gall_list'})
                        # 공지 제외 첫 번째 게시물
                        tr = table.find('tbody').find_all('tr', {'data-type': 'icon_recomimg'})[0]
                        title_td = tr.find('td', {'class': 'gall_tit'})
                        title_anchor = title_td.find_all('a')[0]
                        url = 'https://gall.dcinside.com' + title_anchor.attrs['href']
                        # 마지막으로 스크래핑 했던 URL과 다르면 알림
                        if last_urls[target_name] != url:
                            # URL만 보내고 오픈그래프 렌더링은 디스코드 클라이언트에 맡기기
                            await channel.send(f'{target_name} 개념글 알림 [ {url} ]')
                            # 마지막으로 스크래핑 한 URL 갱신
                            last_urls[target_name] = url

        # 60초 대기
        await asyncio.sleep(60)

# 백그라운드 태스크 생성
bot.loop.create_task(scrapping_task())
# 봇 메인루프 실행
bot.run(token)
